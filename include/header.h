#ifndef HEADER_H
#define HEADER_H

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <pthread.h>
//#include <linux/atomic.h> /*!! CHECK it */
/* user include */
//#include "queue.h"
#define MSG_COUNT 20000

enum {QUEUE_EMPTY, READ_SUCCESS, NO_CLIENTS};

typedef struct message{
    //int actual_message_length;
    char text[128];
} message;

typedef struct queue_item{
//    queue_item * prev;
    struct queue_item * next;
    message msg;
    int owner;
    
} queue_item;

typedef struct queue{
    queue_item *head;
    queue_item *tail;
    pthread_mutex_t mutex;
    struct queue *next;
    //pthread_mutex_t write_mutex;
    //long int members;
} queue;

typedef struct queue_manager{
    int managed_queues;
    queue *queue_list_head;
    queue *queue_list_tail;
    pthread_mutex_t membership_mutex;
} queue_manager;


void to_listen(void *q_manager);
void to_message(void *q_manager);
queue* q_manager_join(queue_manager *q_manager);
int q_manager_quit(queue_manager *q_manager, queue *personal_queue);
int q_manager_read(queue *q_ident, message *msg);
int q_manager_write(queue_manager *q_manager, message *msg);
int slength(char *string);

#endif /* HEADER_H */

