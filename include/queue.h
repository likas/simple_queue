typedef struct message{
    //int actual_message_length;
    char text[128];
} message;

typedef struct queue_item{
//    queue_item * prev;
    struct queue_item * next;
    message msg;
    int owner;
    
} queue_item;

typedef struct queue{
    queue_item *head;
    queue_item *tail;
    pthread_mutex_t mutex;
    struct queue *next;
    //pthread_mutex_t write_mutex;
    //long int members;
} queue;

typedef struct queue_manager{
    int managed_queues;
    queue *queue_list_head;
    queue *queue_list_tail;
    pthread_mutex_t membership_mutex;
} q_manager;

