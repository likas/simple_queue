#include "../include/header.h"
//#include "../include/queue.h"

int main(){
    /* locals */
    pthread_t t_1, /*messenger*/
              t_2,
              t_3,
              t_4; /* listener */
    queue_manager *q_manager = malloc(sizeof(queue_manager));
    /* init locals */
    q_manager->managed_queues = 0;
    q_manager->queue_list_head = NULL;
    q_manager->queue_list_tail = NULL;
    
    /* code */
    /* TODO checking */
    /* q_manager_create(&queue1); */

    /* making a thread for listener */
    if( ( pthread_create(&t_1, NULL, (void*)&to_listen, (void*)q_manager) ) < 0)
    {
        perror("pthread_create listener");
        exit(EXIT_FAILURE);
    }
    /* for messenger */
    if(( pthread_create(&t_2, NULL, (void*)&to_message, (void*)q_manager) ) < 0)
    {
        perror("pthread_create messenger");
        exit(EXIT_FAILURE);
    }

    if(( pthread_create(&t_3, NULL, (void*)&to_listen, (void*)q_manager) ) < 0)
    {
        perror("pthread_create listener");
        exit(EXIT_FAILURE);
    }

    if(( pthread_create(&t_4, NULL, (void*)&to_listen, (void*)q_manager) ) < 0)
    {
        perror("pthread_create listener");
        exit(EXIT_FAILURE);
    }
    
    pthread_join(t_2, NULL);
    pthread_join(t_1, NULL);
    pthread_join(t_3, NULL);
    pthread_join(t_4, NULL);

    return EXIT_SUCCESS;
}
