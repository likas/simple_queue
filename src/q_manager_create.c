#include "../include/header.h"
#include "../include/queue.h"

void q_manager_create(queue *q_ident){
    q_ident->tail = NULL;
    q_ident->head = NULL;
    /*
    q_ident->tail = malloc(sizeof(queue_item));
    (q_ident->tail)->prev = NULL;
    (q_ident->tail)->next = NULL;
    strncpy((q_ident->tail)->msg.text, "null\0", (size_t)5);
    (q_ident->tail)->number = 0;
    */
    q_ident->members = 0;
    q_ident->read_mutex = PTHREAD_MUTEX_INITIALIZER;
    q_ident->write_mutex = PTHREAD_MUTEX_INITIALIZER;
}
