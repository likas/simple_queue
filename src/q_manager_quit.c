#include "../include/header.h"

int q_manager_quit(queue_manager *q_manager, queue *personal_queue){
    /* locals */
    queue *next_queue;
    queue *prev_queue;
    queue_item *to_delete;
    /* init locals */
    /* code */
    next_queue = q_manager->queue_list_head;
    while( next_queue != personal_queue && next_queue != NULL ){
        prev_queue = next_queue;
        next_queue = next_queue->next;
    }
    if( next_queue == NULL ){
        printf("queue list error: no queue found on quit()\n");
    }
    pthread_mutex_lock(&q_manager->membership_mutex);
    if( next_queue->head != next_queue->tail ){
             /* queue_item */
        while( (next_queue->head)->next != NULL ){
            //free( &(((next_queue->head)->next)->msg) );
            to_delete = (next_queue->head);
            next_queue->head = (next_queue->head)->next;
            free( to_delete );
        }
    }
    prev_queue->next = next_queue->next;
    free( next_queue );
    pthread_mutex_unlock( &q_manager->membership_mutex);
    return 0;
}
