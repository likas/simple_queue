#include "../include/header.h"

int q_manager_read(queue *q_ident, message *msg){
    queue_item * next_item;
    next_item = q_ident->head;
    if(q_ident->head == NULL){
        //printf("%p \n %p \n", q_ident, q_ident->head);
        /* that's if no-one calls write on this queue yet */
        return QUEUE_EMPTY;
    }else if((next_item->next) == NULL){
        /* if only one message was writen since last read, we must check if it done writing */
//        ++q_ident->members;
//        pthread_mutex_unlock(&q_ident->read_mutex);
//        next_item = q_ident->head;
/*        while( next_item->next != NULL ){
            strncpy(msg->text, (next_item->msg).text, 128);
            next_item = next_item->next;
        } */
        /* читаем последнюю, с блокировкой потому, что она могла недозаписаться*/
        pthread_mutex_lock(&q_ident->mutex);
        (*msg) = next_item->msg;
//        strncpy(msg->text, (next_item->msg).text, 128);
        pthread_mutex_unlock(&q_ident->mutex);
    }else{
        (*msg) = next_item->msg;
//        strncpy(msg->text, ((q_ident->head)->msg).text, 128);
    }
    queue_item *next_item_next;
    next_item_next = next_item->next;
    free(next_item);
    q_ident->head = next_item_next;
    return READ_SUCCESS;
}
