#include "../include/header.h"

int q_manager_write(queue_manager *q_manager, message *msg){
    /* locals */
    if( q_manager->queue_list_head == NULL ){
        printf("%p \n %p \n", q_manager, q_manager->queue_list_head);
        return NO_CLIENTS;
    }
    printf("%p \n %p \n", q_manager, q_manager->queue_list_head);
    queue *next_queue;
    queue_item *last_item;
    queue_item *next_queue_tail;
    /* init locals */
    last_item = malloc(sizeof(queue_item));
    pthread_mutex_lock(&q_manager->membership_mutex);
    next_queue = q_manager->queue_list_head;
    next_queue_tail = next_queue->tail;
    /* code */
    while( next_queue != NULL ){
    pthread_mutex_lock(&next_queue->mutex);
        if( next_queue_tail == NULL ){
            next_queue_tail = last_item;
            next_queue->head = last_item;
        }else{
            next_queue_tail->next = last_item;
        }
        last_item->next = NULL;
        last_item->msg = (*msg);
        pthread_mutex_unlock(&next_queue->mutex);
        if( next_queue->next == NULL ) break;
        next_queue = next_queue->next;
        next_queue_tail = next_queue->tail;
    }
    pthread_mutex_unlock(&q_manager->membership_mutex);
    return EXIT_SUCCESS;
}
