#include "../include/header.h"

void to_message(void *q_manager){
    /* locals */
    srandom(time(NULL));
    int msg_sent = 0;
    int seconds_left = 0;
    message msg;
    /* init locals */
    msg.text[0] = 0;
    /* code */
        
    for(    ; msg_sent < MSG_COUNT; ++msg_sent)
    {
        msg.text[0] = (char)msg_sent;
        if( q_manager_write(q_manager, &msg) == NO_CLIENTS ){
            printf("no clients yet\n");
        }
        seconds_left = sleep(((double)random()/(double)RAND_MAX) + 1);
        printf("wake\n");
        if(seconds_left > 0){
            printf("Warning: sleep returns %d seconds left\n", seconds_left);
        }
    }
}
